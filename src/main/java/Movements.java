import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Movements {

    private final String pathMovementsCsv;

    public Movements(String pathMovementsCsv) {
        this.pathMovementsCsv = pathMovementsCsv;
    }

    public double getExpenseSum() {
        double result = 0;
        List<String> movements = load();
        for (int i = 1; i < movements.size(); i++) {
            String[] fragments = movements.get(i).split(",");
                result += Double.parseDouble(fragments[7].replaceAll("\"", ""));
        }
        return result;
    }

    public double getIncomeSum() {
        double result = 0;
        List<String> movements = load();
        for (int i = 1; i < movements.size(); i++) {
            String[] fragments = movements.get(i).split(",");
                result += Double.parseDouble(fragments[6].replaceAll("\"", ""));
        }
        return result;
    }

    private List<String> load() {
        List<String> staff = new ArrayList<>();
        try {
            List<String> lines = Files.readAllLines(Paths.get(pathMovementsCsv));
            staff.addAll(lines);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return staff;
    }

    public void expenseInfo() {
        double taxiResult = 0;
        double mobileResult = 0;
        double issResult = 0;
        double KURSCHAVELResult = 0;
        double others = 0;
        List<String> mobile = new ArrayList<>();
        List<String> iss = new ArrayList<>();
        List<String> taxi = new ArrayList<>();
        List<String> KUSCHAVEL = new ArrayList<>();
        List<String> movements = load();
        for (int i = 1; i < movements.size(); i++) {
            String[] fragments = movements.get(i).split(",");
            for (String fragment : fragments) {
                if (fragment.contains("809216")) {
                    mobile.add(fragments[7]);
                } else if (fragment.contains("210106")) {
                    iss.add(fragments[7]);
                } else if (fragment.contains("TAXI")) {
                    taxi.add(fragments[7]);
                } else if (fragment.contains("KUSCHAVEL")) {
                    KUSCHAVEL.add(fragments[7]);
                }
                others = getExpenseSum() - (KURSCHAVELResult + taxiResult + issResult + mobileResult);
            }
        }
        for (String s : taxi) {
            taxiResult += Double.parseDouble(s);
        }
        for (String s : KUSCHAVEL) {
            KURSCHAVELResult += Double.parseDouble(s);
        }
        for (String s : iss) {
            issResult += Double.parseDouble(s);
        }
        for (String s : mobile) {
            mobileResult += Double.parseDouble(s);
        }
        System.out.printf("Сумма расходов: %1$,.1f\nCумма доходов: %2$,.1f\n\n" +
                        "Сумма расходов по организациям:" + "\nYANDEX TAXI: %3$,.1f\n" +
                        "KURCHAVEL: %4$,.1f\nAlfa Iss: %5$,.1f\nALFA_MOBILE: %6$,.1f\n" +
                        "Прочие расходы: %7$,.1f%n",
                getExpenseSum(), getIncomeSum(), taxiResult, KURSCHAVELResult, issResult, mobileResult
                , others);
    }
}